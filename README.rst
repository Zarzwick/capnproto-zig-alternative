==========================================
Non-"official" capnproto generator for zig
==========================================

This is an obsolete project, please look at https://gitlab.com/Zarzwick/capnproto-zig, which is a **mirror** to my personal fossil server.
