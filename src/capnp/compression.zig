const std = @import("std") ;

const bytes_per_word = @import("base.zig").bytes_per_word ;
const zero_word = [bytes_per_word]u8 {0, 0, 0, 0, 0, 0, 0, 0} ;

///
/// Provides writer interfaces.
///
pub fn Compressor(comptime UnderlyingWriter: type) type {
    return struct {
        const Self = @This() ;
        const EncodingError = error { WrongNumberOfBytes } ;
        const Error = UnderlyingWriter.Error || EncodingError ;
        const Writer = std.io.Writer(*Self, anyerror, compress) ;

        underlying_writer : UnderlyingWriter,
        total_bytes_written : usize,
        bytes_written : usize,

        const State = enum { per_word, zeroes_sequence, unpackable } ;

        fn wordIsZero(word: []const u8) bool {
            return std.mem.eql(u8, zero_word[0..], word) ;
        }

        fn wordIsUnpackable(word: []const u8) bool {
            var i : u8 = 0 ;
            while (i < bytes_per_word) : (i += 1) {
                if (word[i] == 0) {
                    return false ;
                }
            }
            return true ;
        }

        fn compressWord(
            self: *Self,
            word: *const [bytes_per_word]u8,
            buffer: *[bytes_per_word+1]u8,
            len: *u8,
        ) !void {
            len.* = 1 ;
            buffer[0] = 0 ;
            comptime var i : u8 = 0 ;
            inline while (i < bytes_per_word) : (i += 1) {
                if (word[i] != 0) {
                    buffer[0] |= (0x1 << i) ;
                    buffer[len.*] = word[i] ;
                    len.* += 1 ;
                }
            }
            self.bytes_written =
                try self.underlying_writer.write(buffer[0..len.*]) ;
            self.total_bytes_written += self.bytes_written ;
        }

        fn compressZeroRegion(
            self: *Self,
            buffer: *[2]u8,
            n: u8,
        ) !void {
            buffer[0] = 0 ;
            buffer[1] = n - 1 ;
            self.bytes_written = try self.underlying_writer.write(buffer[0..2]) ;
            self.total_bytes_written += self.bytes_written ;
        }

        fn compressUnpackedRegion(
            self: *Self,
            bytes: []const u8,
            //buffer: 
        ) !void {
            _ = self ;
            _ = bytes ;
            unreachable ;
        }

        ///
        /// This function is the write function of the compressor. It will write, to
        /// the context writer, the compressed version of bytes. The size returned is
        /// the number of bytes compressed, not the bytes written. This is for it to
        /// work nicely with the writer interface. This might not be a very good
        /// idea, but I'm trying.
        ///
        fn compress(self: *Self, bytes: []const u8) !usize {
            if ((bytes.len % bytes_per_word) != 0) {
                return Self.Error.WrongNumberOfBytes ;
            }
            const words_count = bytes.len / bytes_per_word ;
            var buffer : [bytes_per_word + 1]u8 = undefined ;
            //var window = bytes[0..bytes_per_word] ;
            var state = State.per_word ;
            var len : u8 = 0 ;
            var i : u32 = 0 ;
            while (i < words_count) : (i += 1) {
                const a = bytes_per_word * i ;
                const b = bytes_per_word * (i + 1) ;
                const word = bytes[a..b][0..bytes_per_word] ;
                if (wordIsZero(word)) {
                    switch (state) {
                        .per_word => {
                            len = 0 ;
                            state = State.zeroes_sequence ;
                        },
                        .zeroes_sequence => len += 1,
                        .unpackable => unreachable,
                    }
                } else if (wordIsUnpackable(word)) {
                    try switch (state) {
                        .per_word => unreachable,
                        .zeroes_sequence => unreachable,
                        .unpackable => unreachable,
                    } ;
                } else {
                    switch (state) {
                        .zeroes_sequence => {
                            try self.compressZeroRegion(buffer[0..2], len) ;
                            state = State.per_word ;
                        },
                        .unpackable => unreachable,
                        else => {},
                    }
                    try self.compressWord(word, &buffer, &len) ;
                }
            }
            self.bytes_written = 256 ;
            return bytes.len ;
        }

        pub fn writer(self: *Self) Writer {
            return .{ .context = self } ;
        }
    } ;
}
