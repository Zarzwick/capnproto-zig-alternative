const std       = @import("std") ;
const builtin   = @import("builtin") ;
const base      = @import("base.zig") ;

const Allocator = std.mem.Allocator ;
const ArrayList = std.ArrayList ;
const List      = std.SinglyLinkedList ;

pub const View    = base.View ;
pub const Stencil = base.Stencil ;

pub const Error = error {
    ListTooLong,
    OutOfBounds,
    EmptySegment,
    TooLargeForSegment,
} ;

const bytes_per_word = base.bytes_per_word ;

pub const max_list_len = (0x1 << 30) - 1 ;

// TODO Split this... thing into multiple files.


///
/// Returns the offset between to pointers taken from slices.
///
fn offsetBetween(a: []const u8, b: []const u8) u32 {
    const addr_a = @ptrToInt(&a[0]) ;
    const addr_b = @ptrToInt(&b[0]) ;
    const addr_max = @maximum(addr_a, addr_b) ;
    const addr_min = @minimum(addr_a, addr_b) ;
    const offset = addr_max - addr_min ;
    if (offset > ((1<<32) - 1)) { unreachable ; }
    return @intCast(u32, offset) ;
}

///
/// Enccode a pointer to a struct in the provided slice.
///
pub fn encodeStructPointer(
    data_offset: u32,
    data_size: u32,
    ptr_size: u32,
    bytes: *[bytes_per_word]u8,
) void {
    var word : u64 = 0 ;
    word |= base.Masks.struct_offset  & (@intCast(usize, data_offset) << 2) ;
    word |= base.Masks.struct_datasec & (@intCast(usize, data_size) << 32) ;
    word |= base.Masks.struct_ptrsec  & (@intCast(usize, ptr_size) << 48) ;
    std.mem.writeIntLittle(u64, bytes, word) ;
}

///
/// Encode a far pointer in the provided bytes. If indirection is true, this
/// is the "B = 1" case of the cap'n'proto documention, not supported yet.
///
fn encodeFarPointer(
    landing_pad_seg: u32,
    landing_pad_off: u32,
    bytes: *[bytes_per_word]u8,
    indirection: bool,
) void {
    // TODO Check that the offset fits in 29 bits...
    // TODO Handle the (rare) indirection case.
    if (indirection) { unreachable ; }
    var word: u64 = 2 ;
    const landing_pad_type: u32 = if (indirection) 1 else 0 ;
    word |= base.Masks.farptr_pad    & (@intCast(usize, landing_pad_type) << 2) ;
    word |= base.Masks.farptr_offset & (@intCast(usize, landing_pad_off)  << 3) ;
    word |= base.Masks.farptr_id     & (@intCast(usize, landing_pad_seg) << 32) ;
    std.mem.writeIntLittle(u64, bytes, word) ;
}


pub const Heuristic = union(enum) {
    nostorage: void,
    percentage: u32, // Litteraly in %, discuss before making it a float.
    threshold: u32,
} ;

///
/// A segmented buffer. This is an extensible buffer that can grow segment
/// after segment, keeping the previous segments unchanged to avoid pointer
/// invalidation.
///
/// It is intended to be used as an allocator. It doesn't provide the true
/// Zig interface to Allocators, because it would be pointless, but an alloc
/// function is provided, that scatters data on segments with an heuristic.
///
/// The heuristic is the following : we have regular segments and "storage"
/// segments, dedicated to store objects with a size superior to a certain
/// proportion of the max segment size.
///
/// Do note that this implies it is possible for some data to exist in a
/// segment *preceding* the segment in which the pointer resides. I didn't
/// try to prevent that as is it not specifically forbidden.
///
pub const MessageBuffer = struct {
    max_segment_size: u32 = 1024,
    underlying_allocator: Allocator,
    segments: ArrayList(ArrayList(u8)),
    allocators: ArrayList(FixedAllocator),
    regular_ids: ArrayList(u32),

    /// Controls the behaviour of the allocator. This can totally be changed
    /// during usage. Results will be strange but it is permitted.
    heuristic: Heuristic,

    const Self = @This() ;
    const FixedAllocator = std.heap.FixedBufferAllocator ;

    ///
    /// Initialize the segments with a pre-allocated buffer, and no segment.
    ///
    pub fn init(max_segment_size: u32, allocator: Allocator) !Self {
        var buffer : Self = undefined ;
        buffer.underlying_allocator = allocator ;
        buffer.max_segment_size = max_segment_size ;
        buffer.segments = ArrayList(ArrayList(u8)).init(allocator) ;
        buffer.allocators = ArrayList(FixedAllocator).init(allocator) ;
        buffer.regular_ids = ArrayList(u32).init(allocator) ;
        buffer.heuristic = .{ .percentage = 10 } ;
        _ = try buffer.addSegment() ;
        _ = try buffer.alloc(bytes_per_word, 0, 0) ; // Allocate the first pointer.
        return buffer ;
    }

    ///
    /// Destroys everything.
    ///
    pub fn deinit(self: *Self) void {
        self.regular_ids.deinit() ;
        self.allocators.deinit() ;
        self.segments.deinit() ;
    }

    ///
    /// Append a new segment and corresponding fixed allocator. No
    /// verification is performed regarding the possible emptyness of
    /// already existing segments.
    ///
    pub fn addSegment(self: *Self) !u32 {
        const n = @intCast(u32, self.segments.items.len) ;
        try self.segments.resize(n + 1) ;
        try self.allocators.resize(n + 1) ;
        // Create the new segments buffer.
        self.segments.items[n] = ArrayList(u8).init(self.underlying_allocator) ;
        try self.segments.items[n].resize(self.max_segment_size) ;
        // Instanciate the fixed allocator.
        var segment = self.segments.items[n].items[0..] ;
        self.allocators.items[n] = FixedAllocator.init(segment) ;
        // Add to the regular segments IDs list.
        try self.regular_ids.append(n) ;
        return n ;
    }

    ///
    /// Adds a new segments and records it in the storage IDs list.
    ///
    pub fn addStorageSegment(self: *Self) !u32 {
        const s = self.addSegment() ;
        _ = self.regular_ids.pop() ;
        return s ;
    }

    ///
    /// Shrink segments to match the length of the fixed allocators, since
    /// their current sizes are the maximum segment size, to allow the fixed
    /// allocators to work properly.
    ///
    fn shrinkSegments(self: *Self) void {
        for (self.segments.items) |*segment, i| {
            const len = self.allocators.items[i].end_index ;
            segment.*.shrinkRetainingCapacity(len) ;
        }
    }

    ///
    /// Write segments using the given writer.
    /// TODO Test (I know it works because it's historical copy, but still...)
    ///
    pub fn feedTo(self: *Self, writer: anytype) !void {
        self.shrinkSegments() ;
        // Write segments infos.
        try writer.writeIntLittle(u32, @intCast(u32, self.segments.items.len - 1)) ;
        //std.debug.print("Write {} segments.\n", .{self.segments.items.len}) ;
        for (self.segments.items) |segment| {
            try writer.writeIntLittle(u32, @intCast(u32, segment.items.len / bytes_per_word)) ;
            //std.debug.print("Write segment's length {}.\n", .{segment.items.len / bytes_per_word}) ;
        }
        // Add padding if required.
        if ((self.segments.items.len & 1) == 0) {
            try writer.writeIntLittle(u32, 0) ;
            //std.debug.print("Write padding half word.\n", .{}) ;
        }
        // Write segments.
        for (self.segments.items) |segment| {
            //std.debug.print("Write segment of length {}.\n", .{segment.items.len / bytes_per_word}) ;
            try writer.writeAll(segment.items) ;
        }
    }

    ///
    /// A Pointer stores the information that is common to all pointers in a
    /// message, that is, the address of the pointer, the adress of the content,
    /// and the segment in which the addresses are specified.
    ///
    pub const Pointer = struct {
        ptr_segment: u32,
        data_segment: u32,
        ptr_pos: u32,
        data_pos: u32,
    } ;

    fn candidateForStorage(self: Self, size: u32) bool {
        return switch (self.heuristic) {
            Heuristic.nostorage => false,
            Heuristic.percentage => |p| (size*100/self.max_segment_size) >= p,
            Heuristic.threshold => |t| size >= t,
        } ;
    }

    fn findAdequateStorageSegment(self: *Self, size: u32) !u32 {
        const n = self.segments.items.len ;
        var i: usize = 0 ;
        var j: usize = 0 ;
        // This is valid because the ids are sorted. For the intuition i is
        // simply iterating over the implicitely defined storage ids.
        while (i < n) : (i += 1) {
            while (i <= self.regular_ids.items[j]) : (i += 1) {}
            j += 1 ;
            if (i < n) {
                const remaining = self.max_segment_size -
                    self.allocators.items[i].end_index ;
                if (size <= remaining) {
                    return @intCast(u32, i) ;
                }
            }
        }
        return self.addStorageSegment() ;
    }

    fn findAdequateRegularSegment(self: *Self, size: u32) !u32 {
        for (self.regular_ids.items) |i| {
            const remaining = self.max_segment_size -
                self.allocators.items[i].end_index ;
            if (size <= remaining) {
                return @intCast(u32, i) ;
            }
        }
        return self.addSegment() ;
    }

    ///
    /// Reserves new data on a segment and returns the pointer.
    ///
    pub fn alloc(self: *Self, size: u32, seg: u32, ptr_pos: u32) !Pointer {
        // Check that the alloc request is sound.
        if (size > self.max_segment_size) {
            return Error.TooLargeForSegment ;
        }
        var updated_ptr_segment = seg ;
        var updated_ptr_pos = ptr_pos ;
        var target_segment: u32 = undefined ;
        const for_storage = self.candidateForStorage(size) ;
        const remaining = self.max_segment_size - self.allocators.items[seg].end_index ;
        // If we must allocate on another segment, we must create an inter-
        // segment pointer, and then allocate a new word-sized pointer before
        // allocating the requested size.
        if ((for_storage) or (size > remaining)) {
            // Find the segment...
            target_segment = try
                if (for_storage) self.findAdequateStorageSegment(size)
                else self.findAdequateRegularSegment(size) ;
            // Allocate a landing pointer in it.
            var allocator = self.allocators.items[target_segment].allocator() ;
            const segment_bytes = self.segments.items[target_segment].items[0..1] ;
            const ptr_bytes = try allocator.alloc(u8, bytes_per_word) ;
            updated_ptr_segment = target_segment ;
            updated_ptr_pos = offsetBetween(segment_bytes, ptr_bytes) ;
            // And prepare the inter-segment pointer.
            var far_ptr_bytes = self.segments.items[seg]
                .items[ptr_pos..ptr_pos + bytes_per_word][0..bytes_per_word] ;
            encodeFarPointer(updated_ptr_segment, updated_ptr_pos, far_ptr_bytes, false) ;
        } else {
            target_segment = seg ;
        }
        // Now, in all cases, we can allocate the requested size in target.
        var allocator = self.allocators.items[target_segment].allocator() ;
        const segment_bytes = self.segments.items[target_segment].items[0..1] ;
        const bytes = try allocator.alloc(u8, size) ;
        const data_pos = offsetBetween(segment_bytes, bytes) ;
        // And construct the pointer for it.
        return Pointer
            { .ptr_segment = updated_ptr_segment
            , .data_segment = target_segment
            , .ptr_pos = updated_ptr_pos
            , .data_pos = data_pos
            } ;
    }
} ;

test "message buffer extends correctly" {
    const bw = bytes_per_word ;
    var buffer = try MessageBuffer.init(100*bw, std.heap.c_allocator) ;
    defer buffer.deinit() ;
    buffer.heuristic = .{ .percentage = 10 } ;
    // Allocate the first word.
    var ptr = try buffer.alloc(1*bw, 0, 0) ;
    // Allocate bytes on the first segment.
    ptr = try buffer.alloc(3*bytes_per_word, 0, 0) ;
    ptr = try buffer.alloc(4*bytes_per_word, 0, 0) ;
    try std.testing.expect(buffer.segments.items.len == 1) ;
    try std.testing.expect(buffer.allocators.items[0].end_index == 8*bw) ;
    // Allocate bytes that should go on storage.
    ptr = try buffer.alloc(10*bw, 0, 0) ;
    ptr = try buffer.alloc(50*bw, 0, 0) ;
    try std.testing.expect(buffer.segments.items.len == 2) ;
    try std.testing.expect(buffer.regular_ids.items.len == 1) ;
    try std.testing.expect(buffer.regular_ids.items[0] == 0) ;
    // Allocate 72 words on the first segment.
    var i: u32 = 0 ; while (i < 8) : (i += 1) {
        ptr = try buffer.alloc(9*bytes_per_word, 0, 0) ;
    }
    try std.testing.expect(buffer.segments.items.len == 2) ;
    try std.testing.expect(buffer.allocators.items[0].end_index == 80*bw) ;
    // Trigger another storage segment.
    ptr = try buffer.alloc(39*bw, 0, 0) ;
    try std.testing.expect(buffer.segments.items.len == 3) ;
    try std.testing.expect(buffer.regular_ids.items.len == 1) ;
    try std.testing.expect(buffer.allocators.items[1].end_index == 62*bw) ;
    try std.testing.expect(buffer.allocators.items[2].end_index == 40*bw) ;
    // Trigger another regular segment by changing the heuristic.
    buffer.heuristic = .{ .nostorage = {} } ;
    ptr = try buffer.alloc(21*bw, 0, 0) ;
    buffer.heuristic = .{ .percentage = 10 } ;
    try std.testing.expect(buffer.segments.items.len == 4) ;
    try std.testing.expect(buffer.regular_ids.items.len == 2) ;
    try std.testing.expect(buffer.regular_ids.items[1] == 3) ;
    try std.testing.expect(buffer.allocators.items[3].end_index == 22*bw) ;
    // Assess that the first segment is still filled for a small enough alloc.
    ptr = try buffer.alloc(7*bw, 0, 0) ;
    ptr = try buffer.alloc(7*bw, 0, 0) ;
    try std.testing.expect(buffer.segments.items.len == 4) ;
    try std.testing.expect(buffer.regular_ids.items.len == 2) ;
    try std.testing.expect(buffer.allocators.items[0].end_index == 94*bw) ;
    try std.testing.expect(buffer.allocators.items[3].end_index == 22*bw) ;
}

test "message buffer pointer correctness" {
    const bw = bytes_per_word ;
    var buffer = try MessageBuffer.init(100*bw, std.heap.c_allocator) ;
    defer buffer.deinit() ;
    buffer.heuristic = .{ .percentage = 10 } ;
    // Allocate the first word.
    var ptr = try buffer.alloc(1*bw, 0, 0) ;
    // Allocate various regions in the current segment and check pointers.
    ptr = try buffer.alloc(4*bw, 0, 0*bw) ; try std.testing.expect(ptr.ptr_segment == 0 and ptr.ptr_pos == 0*bw and ptr.data_segment == 0 and ptr.data_pos == 1*bw) ;
    ptr = try buffer.alloc(1*bw, 0, 1*bw) ; try std.testing.expect(ptr.ptr_segment == 0 and ptr.ptr_pos == 1*bw and ptr.data_segment == 0 and ptr.data_pos == 5*bw) ;
    ptr = try buffer.alloc(1*bw, 0, 2*bw) ; try std.testing.expect(ptr.ptr_segment == 0 and ptr.ptr_pos == 2*bw and ptr.data_segment == 0 and ptr.data_pos == 6*bw) ;
    ptr = try buffer.alloc(1*bw, 0, 3*bw) ; try std.testing.expect(ptr.ptr_segment == 0 and ptr.ptr_pos == 3*bw and ptr.data_segment == 0 and ptr.data_pos == 7*bw) ;
    ptr = try buffer.alloc(1*bw, 0, 4*bw) ; try std.testing.expect(ptr.ptr_segment == 0 and ptr.ptr_pos == 4*bw and ptr.data_segment == 0 and ptr.data_pos == 8*bw) ;
    // Allocate various regions in other segments and check pointers.
    ptr = try buffer.alloc(12*bw, 0, 5*bw) ; try std.testing.expect(ptr.ptr_segment == 1 and ptr.ptr_pos == 0*bw  and ptr.data_segment == 1 and ptr.data_pos == 1*bw) ;
    ptr = try buffer.alloc(24*bw, 0, 6*bw) ; try std.testing.expect(ptr.ptr_segment == 1 and ptr.ptr_pos == 13*bw and ptr.data_segment == 1 and ptr.data_pos == 14*bw) ;
}


//pub const ListStencil

// ///
// /// Returns the stencils type associated with a given type.
// ///
//pub fn Stencil(comptime T: type) type {
//    return switch (T) {
//        bool, i8, u8, i16, u16, i32, u32, i64, u64 => T,
//        else => T.Stencil
//    } ;
//}

///
/// Set the memory region corresponding to a field given a reference value.
/// The offset is *provided in bits* when the type is bool.
///
pub fn set(comptime T: type, value: T, bytes: []u8, offset: u64) !void {
    if (T == bool) {
        // Offset is in bits.
        const offset_bytes = @divTrunc(offset, 8) ;
        const rem_bits = @intCast(u3, @rem(offset, 8)) ;
        const one: u8 = 1 ;
        if (value) {
            bytes[offset_bytes] |= (one << rem_bits) ;
        } else {
            bytes[offset_bytes] &= (~(one << rem_bits)) ;
        }
        return ;
    }
    var dest = bytes[offset..] ;
    switch (T) {
        f32 => {
            const int = @bitCast(u32, value) ;
            std.mem.writeIntLittle(u32, dest[0..@sizeOf(T)], int) ;
        },
        f64 => {
            const int = @bitCast(u64, value) ;
            std.mem.writeIntLittle(u64, dest[0..@sizeOf(T)], int) ;
        },
        i8, i16, i32, i64, u8, u16, u32, u64 => {
            std.mem.writeIntLittle(T, dest[0..@sizeOf(T)], value) ;
        },
        else => unreachable,
    }
}

///
/// This function will check bounds and read the various types from the
/// buffer. All the generated getters will in fact call this function.
/// The offset is *provided in bits* when the type is bool.
///
pub fn get(comptime T: type, bytes: []const u8, offset: u64) !T {
    // TODO Check bounds ^^
    if (T == bool) {
        // Offset is in bits.
        const offset_bytes = @divTrunc(offset, 8) ;
        const rem_bits = @intCast(u3, @rem(offset, 8)) ;
        const one: u8 = 1 ;
        return (bytes[offset_bytes] & (one << rem_bits)) != 0 ;
    }
    var source = bytes[offset..] ;
    switch (T) {
        f32 => {
            const int = std.mem.readIntLittle(u32, source) ;
            return @bitCast(T, int) ;
        },
        f64 => {
            const int = std.mem.readIntLittle(u64, source) ;
            return @bitCast(T, int) ;
        },
        i8, i16, i32, i64, u8, u16, u32, u64 => {
            return std.mem.readIntLittle(T, source) ;
        },
        else => unreachable,
    }
}
