#include <vector>
#include <string>
#include <locale>
#include <cassert>
#include <fstream>
#include <iostream>
#include <optional>
#include <unistd.h>

#include <capnp/schema.h>
#include <capnp/serialize.h>
#include <capnp/schema.capnp.h>
#include <capnp/schema-loader.h>

using namespace std ;
using namespace capnp ;
using namespace capnp::schema ;

using uint = unsigned int ;
using Request = CodeGeneratorRequest ;
using ReqFile = schema::CodeGeneratorRequest::RequestedFile ;

//static constexpr uint64_t NAME_ANNOTATION_ID = 0xf264a779fef191ceull ;
const char tab[] = "    " ;

#include "conversions.c++" // Bad, bad Hugo...

///
/// Possible error codes.
///
enum class ErrorCode
    { Ok = 0
    , Failed
    } ;

static_assert(schema::Type::VOID == 0, "Type enumerant doesn't start at 0.") ;
const char* type_names[20] =
    { "void"
    , "bool"
    , "i8"
    , "i16"
    , "i32"
    , "i64"
    , "u8"
    , "u16"
    , "u32"
    , "u64"
    , "f32"
    , "f64"
    , "[]const u8"
    , "[]const u8"
    , "List"
    , "Enum"
    , "Struct"
    , "Interface"
    , "AnyPointer"
    , "ListOfList"
    } ;

string typeName(const capnp::Type& type)
{
    // Detect the special case of a list of lists.
    if (type.which() == capnp::schema::Type::LIST) {
        if (type.asList().getElementType().isList()) {
            //cerr << "// A list of list is detected, emitting ListOfList instead of List." << endl ;
            return type_names[type.which() + 5] ;
        }
    }
    // Master switch.
    switch (type.which()) {
        case capnp::schema::Type::INTERFACE:
        case capnp::schema::Type::ANY_POINTER:
            return { "placeholdertoken" } ;
        case capnp::schema::Type::ENUM:
            return string(type.asEnum().getShortDisplayName().cStr()) ;
        default:
            return { type_names[type.which()] } ;
    }
}

string nodeName(const capnp::Schema& schema) {
    const auto display_name = schema.getProto().getDisplayName() ;
    const auto prefix_len = schema.getProto().getDisplayNamePrefixLength() ;
    const auto len = display_name.size() ;
    const char* c_str = display_name.cStr() ;
    return string(c_str + prefix_len, len - prefix_len) ;
}

bool typeIsPrimitive(const capnp::Type& type)
{
    switch (type.which()) {
        case capnp::schema::Type::ENUM:
        case capnp::schema::Type::STRUCT:
        case capnp::schema::Type::INTERFACE:
        case capnp::schema::Type::ANY_POINTER:
        case capnp::schema::Type::LIST:
            return false ;
        default:
            return true ;
    }
}

void printTypeInfos(const capnp::Type& type)
{
    cerr << "Type : " ;
    switch (type.which()) {
        case capnp::schema::Type::ENUM:
        case capnp::schema::Type::STRUCT:
            cerr << "struct" << endl ;
            break ;
        case capnp::schema::Type::INTERFACE:
            cerr << "interface" << endl ;
            break ;
        case capnp::schema::Type::ANY_POINTER:
            cerr << "void*" << endl ;
            break ;
        default:
            cerr << type_names[type.which()] << endl ;
    }
}

// Forward declare.
void emitNestedNodes
    ( ostream& os
    , const SchemaLoader& loader
    , const Schema& schema
    , const uint level = 0
    ) ;

void emitUnionEnum
    ( ostream& os
    , const SchemaLoader& loader
    , const Schema& schema
    , const string& indent
    )
{
    const auto union_fields = schema.asStruct().getUnionFields() ;
    if (union_fields.size() > 0) {
        os << indent << "pub const CapnpUnionWhich = enum(u16) {" << endl ;
        uint i = 0 ;
        for (const auto& field: union_fields) {
            os << indent << tab << field.getProto().getName().cStr() << " = " << std::to_string(i) << "," << endl ;
            ++ i ;
        }
        os << indent << "} ;" << endl ;
    }
}

void emitUnionFunctions
    ( ostream& os
    , const SchemaLoader& loader
    , const StructSchema& schema
    , const string& indent
    , const char* // name
    )
{
    const auto union_fields = schema.asStruct().getUnionFields() ;
    if (union_fields.size() > 0) {
        uint i = 0 ;
        os << indent << tab << "pub fn which(self: @This()) @This().CapnpUnionWhich {" << endl ;
        os << indent << tab << tab << "const bytes = self.message.segments.items[self.segment].items[self.data..] ;" << endl ; // FIXME
        os << indent << tab << tab << "const discriminant = capnp.serialization.get(u16, bytes, " << schema.getProto().getStruct().getDiscriminantOffset() * 2 << ") ;" << endl ;
        os << indent << tab << tab << "return @intToEnum(@This().CapnpUnionWhich, discriminant) ;" << endl ;
        os << indent << tab << "}" << endl ;
        for (const auto& field: union_fields) {
            const auto enumname = field.getProto().getName().cStr() ;
            const auto name = uppercaseFirstLetter(enumname) ;
            os << indent << tab << "pub fn is" << name << "(self: @This()) bool {" << endl ;
            os << indent << tab << tab << "return self.which() == @This().CapnpUnionWhich." << enumname << " ;" << endl ;
            os << indent << tab << "}" << endl ;
            ++ i ;
        }
    }
}

///
/// Emit a basic prologue to the zig file.
///
void emitPrologue(ostream& os)
{
    os << "const std   = @import(\"std\") ;" << endl ;
    os << "const capnp = @import(\"capnp\") ;" << endl ;
    os << endl ;
    /*
    os << "//const StructView = capnp.deserialization.StructView ;" << endl ; // FIXME, REMOVE
    os << "//const ListView   = capnp.deserialization.ListView ;" << endl ; // FIXME, REMOVE
    os << endl ;
    os << "//const StructStencil = capnp.serialization.StructStencil ;" << endl ;
    os << "//const ListStencil   = capnp.serialization.ListStencil ;" << endl ;
    os << endl ;
    */
    os << "const bytes_per_word = capnp.bytes_per_word ;" << endl ;
    os << endl ;
    //os << "const Array     = std.ArrayList ;" << endl ;
    //os << endl ;
}

namespace reader {

void emitFieldGetter
    ( ostream& os
    , const SchemaLoader& loader
    , const capnp::StructSchema::Field& field
    , const string& indent
    )
{
    const auto primitive = typeIsPrimitive(field.getType()) ;
    const auto proto = field.getProto() ;
    const auto name = uppercaseFirstLetter(proto.getName().cStr()) ;
    const auto type = typeName(field.getType()) + (primitive ? "" : "View") ;
    auto pos_expr = string("capnp.bytes_per_word * (self.struct_view.content_pos") ;
    if (primitive) {
        pos_expr += ") + @sizeOf(" + type + ") * " + std::to_string(proto.getSlot().getOffset()) ;
    } else {
        const auto parent_proto = field.getContainingStruct().getProto() ;
        pos_expr += " + " + std::to_string(parent_proto.getStruct().getDataWordCount()) ;
        pos_expr += ") + @sizeOf(" + type + ") * " + std::to_string(proto.getSlot().getOffset()) ;
    }
    os << indent << "pub fn get" << name << "(self: @This(), message: *capnp.MessageStream) !" << type << " {" << endl ;
    os << indent << tab << "const pos = " << pos_expr << " ;" << endl ;
    os << indent << tab << "return capnp.deserialization.getInternal(" << type << ", message.*.stream, pos) ;" << endl ;
    os << indent << "}" << endl ;
}

///
/// Essentially the same as emitStructFunctions, but without an init function.
///
void emitGroupFunctions
    ( ostream& os
    , const SchemaLoader& loader
    , const StructSchema& schema
    , const string& indent
    , const char* name
    )
{
    assert(schema.getProto().getStruct().getIsGroup()) ;
    for (const auto& field: schema.getFields()) {
        const auto fproto = field.getProto() ;
        if (fproto.which() == Field::SLOT) {
            emitFieldGetter(os, loader, field, indent + tab) ;
        } else if (fproto.which() == Field::GROUP) {
            // FIXME See code in the writer.
        }
    }
}

///
/// ...
///
void emitStructFunctions
    ( ostream& os
    , const SchemaLoader& loader
    , const StructSchema& schema
    , const string& indent
    , const char* name
    )
{
    /*
    os << indent << tab << "pub fn init(message: *capnp.MessageStream) !Self {" << endl ;
    os << indent << tab << tab << "return Self" << endl ;
    os << indent << tab << tab << tab << "{ .struct_view = try StructView.init(message.*.stream)" << endl ;
    os << indent << tab << tab << tab << "} ;" << endl ;
    os << indent << tab << "}" << endl ;
    for (const auto& field: schema.getFields()) {
        const auto fproto = field.getProto() ;
        if (fproto.which() == Field::SLOT) {
            emitFieldGetter(os, field, indent + tab) ;
        } else if (fproto.which() == Field::GROUP) {
            const string name = uppercaseFirstLetter(fproto.getName().cStr()) ;
            os << indent << tab << "pub const " << name << " = struct {" << endl ;
            emitGroupFunctions(os, field.getType().asStruct(), indent + tab, name.c_str()) ;
            os << indent << tab << "} ;" << endl ;
        }
    }
    */
}

} // end namespace reader

namespace writer {

/*
void emitListInit
    ( ostream& os
    , const capnp::StructSchema::Field& field
    , const string& indent
    )
{
    const auto proto = field.getProto() ;
    const auto name = uppercaseFirstLetter(proto.getName().cStr()) ;
    const auto type = typeName(field.getType()) ;
    const auto subtype = is_list_of_list
        ? typeName(field.getType().asList().getElementType().asList().getElementType())
        : typeName(field.getType().asList().getElementType()) ;
    const auto full_type = type + "Stencil(" + subtype + ")" ;
    os << indent << "pub fn init" << name << "(self: Self, n: u32) !" << full_type << " {" << endl ;
    os << indent << tab << "if (n > capnp.serialization.max_list_len) {" << endl ;
    os << indent << tab << tab << "return capnp.serialization.Error.ListTooLong ;" << endl ;
    os << indent << tab << "}" << endl ;
    os << indent << tab << "var bytes_for_ptr = self.struct_stencil.ptr[" ;
    os << proto.getSlot().getOffset() << "*bytes_per_word.." << proto.getSlot().getOffset()+1 << "*bytes_per_word] ;" << endl ;
    os << indent << tab << "return try " << full_type << ".init(n, @sizeOf(" << subtype << "), bytes_for_ptr, self.message) ;" << endl ;
    os << indent << "}" << endl ;
}

void emitListOfListInit
    ( ostream& os
    , const capnp::StructSchema::Field& field
    , const string& indent
    )
{
    const auto proto = field.getProto() ;
    const auto name = uppercaseFirstLetter(proto.getName().cStr()) ;
    const auto type = typeName(field.getType()) ;
    const auto subtype = is_list_of_list
        ? typeName(field.getType().asList().getElementType().asList().getElementType())
        : typeName(field.getType().asList().getElementType()) ;
    const auto full_type = type + "Stencil(" + subtype + ")" ;
    os << indent << "pub fn init" << name << "(self: Self, n: u32) !" << full_type << " {" << endl ;
    os << indent << tab << "if (n > capnp.serialization.max_list_len) {" << endl ;
    os << indent << tab << tab << "return capnp.serialization.Error.ListTooLong ;" << endl ;
    os << indent << tab << "}" << endl ;
    os << indent << tab << "var bytes_for_ptr = self.struct_stencil.ptr[" ;
    os << proto.getSlot().getOffset() << "*bytes_per_word.." << proto.getSlot().getOffset()+1 << "*bytes_per_word] ;" << endl ;
    os << indent << tab << "return try " << full_type << ".init(n, @sizeOf(" << subtype << "), bytes_for_ptr, self.message) ;" << endl ;
    os << indent << "}" << endl ;
}

void emitStructInit
    ( ostream& os
    , const capnp::StructSchema::Field& field
    , const string& indent
    )
{
}
*/

void emitPrimitiveSet
    ( ostream& os
    , const SchemaLoader& loader
    , const capnp::StructSchema::Field& field
    , const string& indent
    )
{
    const auto proto = field.getProto() ;
    const auto name = uppercaseFirstLetter(proto.getName().cStr()) ;
    const auto type = typeName(field.getType()) ;
    const auto pos_expr = "@sizeOf(" + type + ") * " + std::to_string(proto.getSlot().getOffset()) ;
    os << indent << "pub fn set" << name << "(self: @This(), value: " << type << ") !void {" << endl ;
    os << indent << tab << "var bytes = self.message.segments.items[self.segment].items[self.data..self.ptrs] ;" << endl ;
    if (field.getType() == schema::Type::BOOL) {
        os << indent << tab << "const offset = " << std::to_string(proto.getSlot().getOffset()) << " ;" << endl ;
    } else {
        os << indent << tab << "const offset = @sizeOf(" << type << ") * " << std::to_string(proto.getSlot().getOffset()) << " ;" << endl ;
    }
    if (field.getType().which() == schema::Type::Which::ENUM) {
        os << indent << tab << "return capnp.serialization.set(u16, @enumToInt(value), bytes, offset) ;" << endl ;
    } else {
        os << indent << tab << "return capnp.serialization.set(" << type << ", value, bytes, offset) ;" << endl ;
    }
    os << indent << "}" << endl ;
}

void emitPrimitiveGet
    ( ostream& os
    , const SchemaLoader& loader
    , const capnp::StructSchema::Field& field
    , const string& indent
    )
{
    const auto proto = field.getProto() ;
    const auto name = uppercaseFirstLetter(proto.getName().cStr()) ;
    const auto type = typeName(field.getType()) ;
    os << indent << "pub fn get" << name << "(self: @This()) !" << type << "{" << endl ;
    os << indent << tab << "var bytes = self.message.segments.items[self.segment].items[self.data..self.ptrs] ;" << endl ;
    if (field.getType() == schema::Type::BOOL) {
        os << indent << tab << "const offset = " << std::to_string(proto.getSlot().getOffset()) << " ;" << endl ;
    } else {
        os << indent << tab << "const offset = @sizeOf(" << type << ") * " << std::to_string(proto.getSlot().getOffset()) << " ;" << endl ;
    }
    if (field.getType().which() == schema::Type::Which::ENUM) {
        os << indent << tab << "return @intToEnum(" << type << ", try capnp.serialization.get(u16, bytes, offset)) ;" << endl ;
    } else {
        os << indent << tab << "return capnp.serialization.get(" << type << ", bytes, offset) ;" << endl ;
    }
    os << indent << "}" << endl ;
}

void emitPrimitiveFunctions
    ( ostream& os
    , const SchemaLoader& loader
    , const capnp::StructSchema::Field& field
    , const string& indent
    )
{
    emitPrimitiveSet(os, loader, field, indent) ;
    emitPrimitiveGet(os, loader, field, indent) ;
}

void emitFieldStructInit
    ( ostream& os
    , const SchemaLoader& loader
    , const capnp::StructSchema::Field& field
    , const string& indent
    )
{
    const auto proto = field.getProto() ;
    const auto name = uppercaseFirstLetter(proto.getName().cStr()) ;
    const auto type = nodeName(field.getType().asStruct()) ;
    os << indent << "pub fn init" << name << "(self: @This()) !" << type << ".Stencil {" << endl ;
    os << indent << tab << "const pos: u32 = self.ptrs + " << proto.getSlot().getOffset() << " * bytes_per_word ;" << endl ;
    os << indent << tab << "return " << type << ".Stencil.initFrom(self.message, self.segment, pos) ;" << endl ;
    os << indent << "}" << endl ;
}

void emitFieldStructSet
    ( ostream& os
    , const SchemaLoader& loader
    , const capnp::StructSchema::Field& field
    , const string& indent
    )
{
    cerr << indent << "// set :: Stencil -> ()" << endl ;
    /*
    const auto proto = field.getProto() ;
    const auto parent_proto = field.getContainingStruct().getProto() ;
    const auto name = uppercaseFirstLetter(proto.getName().cStr()) ;
    const auto type = nodeName(field.getType().asStruct()) ;
    const auto pos_expr = string("capnp.bytes_per_word * ((self.struct_view.content_pos) + ") +
            std::to_string(parent_proto.getStruct().getDataWordCount()) +
            ") + @sizeOf(" + type + ") * " +
            std::to_string(proto.getSlot().getOffset()) ;
    os << indent << "pub fn set" << name << "(self: @This(), source: " << type << ".View) !void {" << endl ;
    os << indent << tab << "const pos = " << pos_expr << " ;" << endl ;
    os << indent << tab << "return capnp.serialization.setInternal(" << type << ", source, self.struct_stencil.data, pos) ;" << endl ;
    os << indent << "}" << endl ;
    */
}

void emitFieldStructGet
    ( ostream& os
    , const SchemaLoader& loader
    , const capnp::StructSchema::Field& field
    , const string& indent
    )
{
    cerr << indent << "// get :: () -> Stencil" << endl ;
    /*
    const auto proto = field.getProto() ;
    const auto parent_proto = field.getContainingStruct().getProto() ;
    const auto name = uppercaseFirstLetter(proto.getName().cStr()) ;
    const auto type = nodeName(field.getType().asStruct()) ;
    const auto pos_expr = string("capnp.bytes_per_word * ((self.struct_view.content_pos) + ") +
            std::to_string(parent_proto.getStruct().getDataWordCount()) +
            ") + @sizeOf(" + type + ") * " +
            std::to_string(proto.getSlot().getOffset()) ;
    os << indent << "pub fn get" << name << "(self: @This()) !" << type << ".Stencil {" << endl ;
    os << indent << tab << "const pos = " << pos_expr << " ;" << endl ;
    os << indent << tab << "return capnp.serialization.getInternal(" << type << ", self.struct_stencil.data, pos) ;" << endl ;
    os << indent << "}" << endl ;
    */
}

void emitFieldStructHas
    ( ostream& os
    , const SchemaLoader& loader
    , const capnp::StructSchema::Field& field
    , const string& indent
    )
{
    const auto proto = field.getProto() ;
    const auto name = uppercaseFirstLetter(proto.getName().cStr()) ;
    os << indent << "pub fn has" << name << "(self: @This()) !bool {" << endl ;
    os << indent << tab << "_ = self ;" << endl ;
    os << indent << tab << "// TODO." << endl ;
    os << indent << "}" << endl ;
}

void emitFieldStructAdopt
    ( ostream& os
    , const SchemaLoader& loader
    , const capnp::StructSchema::Field& field
    , const string& indent
    )
{
    cerr << indent << "// adopt (TODO, ORPHANS)." << endl ;
}

void emitFieldStructDisown
    ( ostream& os
    , const SchemaLoader& loader
    , const capnp::StructSchema::Field& field
    , const string& indent
    )
{
    cerr << indent << "// disown (TODO, ORPHANS)." << endl ;
}

void emitFieldStructFunctions
    ( ostream& os
    , const SchemaLoader& loader
    , const capnp::StructSchema::Field& field
    , const string& indent
    )
{
    emitFieldStructInit(os, loader, field, indent) ;
    //emitFieldStructSet(os, loader, field, indent) ;
    //emitFieldStructGet(os, loader, field, indent) ;
    //emitFieldStructHas(os, loader, field, indent) ;
    //emitFieldStructAdopt(os, loader, field, indent) ;
    //emitFieldStructDisown(os, loader, field, indent) ;
}

void emitListFunctions
    ( ostream& os
    , const SchemaLoader& loader
    , const capnp::StructSchema::Field& field
    , const string& indent
    )
{
    //emitListInit(os, field, indent) ;
    //emitListSet() ;
    //emitListGet() ;
    cerr << endl << "// TODO List functions..." << endl << endl ;
}

void emitListOfListsFunctions
    ( ostream& os
    , const SchemaLoader& loader
    , const capnp::StructSchema::Field& field
    , const string& indent
    )
{
    //emitListOfListInit(os, field, indent) ;
    cerr << endl << "// TODO ListOfList functions..." << endl << endl ;
}

void emitListOfStructsFunctions
    ( ostream& os
    , const SchemaLoader& loader
    , const capnp::StructSchema::Field& field
    , const string& indent
    )
{
    //emitListInit(os, field, indent) ;
    cerr << endl << "// TODO ListOfStructs functions..." << endl << endl ;
}

void emitFieldFunctions
    ( ostream& os
    , const SchemaLoader& loader
    , const capnp::StructSchema::Field& field
    , const string& indent
    )
{
    if (field.getType().isList()) {
        if (field.getType().asList().getElementType().isList()) {
            emitListOfListsFunctions(os, loader, field, indent) ;
        } else if (field.getType().asList().getElementType().isStruct()) {
            emitListOfStructsFunctions(os, loader, field, indent) ;
        } else {
            emitListFunctions(os, loader, field, indent) ;
        }
    } else if (field.getType().isStruct()) {
        emitFieldStructFunctions(os, loader, field, indent) ; // TODO Find a better name...
    } else {
        emitPrimitiveFunctions(os, loader, field, indent) ;
    }
}

///
/// Emit the function of structs that determines their size, allocate it on
/// the buffer, and initialize the members.
///
void emitStructInitFromFunction
    ( ostream& os
    , const SchemaLoader&
    , const StructSchema& schema
    , const string& indent
    , const char* name
    )
{
    const auto proto = schema.getProto() ;
    os << indent << tab << "pub fn initFrom(buffer: *capnp.MessageBuffer, ptr_seg: u32, ptr_pos: u32) !@This() {" << endl ;
    const auto data_wc = proto.getStruct().getDataWordCount() ;
    const auto ptrs_wc = proto.getStruct().getPointerCount() ;
    // Ask the buffer to allocate some space.
    os << indent << tab << tab << "const ptr = try buffer.alloc(" ;
    os << (data_wc + ptrs_wc) << " * bytes_per_word, ptr_seg, ptr_pos) ;" << endl ;
    // Get some useful constants.
    os << indent << tab << tab << "const s = ptr.data_segment ;" << endl ;
    os << indent << tab << tab << "const p = ptr.data_pos ;" << endl ;
    os << indent << tab << tab << "const data_section_size = @intCast(u32, " << data_wc << " * bytes_per_word) ;" << endl ;
    os << indent << tab << tab << "var data_section = buffer.segments.items[s].items[p..p + data_section_size] ;" << endl ;
    os << indent << tab << tab << "const q = p + data_section_size ;" << endl ;
    os << indent << tab << tab << "const ptrs_section_size = " << ptrs_wc << " * bytes_per_word ;" << endl ;
    os << indent << tab << tab << "var ptrs_section = buffer.segments.items[s].items[q..q + ptrs_section_size] ;" << endl ;
    // Encode the pointer.
    //os << indent << tab << tab << "std.debug.print(\"(s: {}, p: {}, q: {})\\n\", .{s, p, q}) ;" << endl ;
    //os << indent << tab << tab << "std.debug.print(\"({}, {})\\n\", .{ptr.ptr_segment, ptr.ptr_pos}) ;" << endl ;
    os << indent << tab << tab << "var ptr_word = buffer.segments.items[ptr.ptr_segment].items[ptr.ptr_pos..][0..bytes_per_word] ;" << endl ;
    os << indent << tab << tab << "capnp.serialization.encodeStructPointer((p - ptr_pos - 1)/bytes_per_word, data_section_size/bytes_per_word, ptrs_section_size/bytes_per_word, ptr_word) ;" << endl ;
    // Initialize members to their default values.
    // TODO This is a complex topic.
    os << indent << tab << tab << "for (data_section) |*byte| { byte.* = 0 ; }" << endl ;
    /*
    for (const auto& field: schema.getFields()) {
        const auto fproto = field.getProto() ;
        if (fproto.which() == Field::SLOT) {
            // ....
        }
    }
    */
    // Initialize pointers to zero.
    os << indent << tab << tab << "for (ptrs_section) |*byte| { byte.* = 0 ; }" << endl ;
    // Return the stencil and conclude.
    os << indent << tab << tab << "return @This() { .message = buffer, .segment = s, .data = p, .ptrs = q } ;" << endl ;
    os << indent << tab << "}" << endl ;
}

///
/// Essentially the same as emitStructFunctions, but without an init function.
///
void emitGroupFunctions
    ( ostream& os
    , const SchemaLoader& loader
    , const StructSchema& schema
    , const string& indent
    , const char* name
    )
{
    for (const auto& field: schema.getFields()) {
        const auto fproto = field.getProto() ;
        if (fproto.which() == Field::SLOT) {
            emitFieldFunctions(os, loader, field, indent + tab) ;
        } else if (fproto.which() == Field::GROUP) {
            const string name = uppercaseFirstLetter(fproto.getName().cStr()) ;
            os << indent << tab << "pub const " << name << " = struct {" << endl ;
            os << indent << tab << tab << "message: *capnp.MessageBuffer," << endl ;
            os << indent << tab << tab << "segment: u32," << endl ;
            os << indent << tab << tab << "data: u32," << endl ;
            emitUnionEnum(os, loader, field.getType().asStruct(), indent + tab + tab) ;
            emitGroupFunctions(os, loader, field.getType().asStruct(), indent + tab, name.c_str()) ;
            os << indent << tab << "} ;" << endl ;
            os << indent << tab << "pub fn get" << name << "(self: *@This()) " << name << " {" << endl ;
            os << indent << tab << tab << "return " << name << endl ;
            os << indent << tab << tab << tab << "{ .message = self.message " << endl ;
            os << indent << tab << tab << tab << ", .segment = self.segment " << endl ;
            os << indent << tab << tab << tab << ", .data    = self.data " << endl ;
            os << indent << tab << tab << tab << "} ;" << endl ;
            os << indent << tab << "}" << endl ;
        }
    }
    emitUnionFunctions(os, loader, schema, indent, NULL) ;
}

///
/// A small API tweak. TODO Ideally this should be active only for the type
/// of the root struct, but this is not given from the schema itself.
///
void emitStructInitAsRootFunction
    ( ostream& os
    , const SchemaLoader&
    , const StructSchema&
    , const string& indent
    , const char*
    )
{
    os << indent << tab << "pub fn initAsRoot(buffer: *capnp.MessageBuffer) !@This() {" << endl ;
    os << indent << tab << tab << "return initFrom(buffer, 0, 0) ;" << endl ;
    os << indent << tab << "}" << endl ;
}

///
/// ...
///
void emitStructFunctions
    ( ostream& os
    , const SchemaLoader& loader
    , const StructSchema& schema
    , const string& indent
    , const char* name
    )
{
    emitStructInitFromFunction(os, loader, schema, indent, name) ;
    emitStructInitAsRootFunction(os, loader, schema, indent, name) ;
    emitGroupFunctions(os, loader, schema, indent, name) ;
}

} // end namespace writer


///
/// Emit a structure as well as its reading / writing functions. This
/// is a recursive function. The passed node is guaranteed to be of
/// type struct.
///
void emitStruct
    ( ostream& os
    , const SchemaLoader& loader
    , const Schema& schema
    , const char* name
    , const uint level
    )
{
    const auto structschema = schema.asStruct() ;
    const auto proto = schema.getProto() ;
    // Some "I don't know what I'm doing" check...
    if (proto.getStruct().getIsGroup()) {
        cerr << "Error, there is a group (" << proto.getDisplayName().cStr() << ") which should *not* appear here." << endl ;
        return ;
    }
    /*
    {
        cerr << "// Emitting a struct (" << proto.getDisplayName().cStr() << ") with :" << endl ;
        cerr << "//     Data section : " << proto.getStruct().getDataWordCount() << endl ;
        cerr << "//     Pointer section : " << proto.getStruct().getPointerCount() << endl ;
        cerr << "//     Discriminant count : " << proto.getStruct().getDiscriminantCount() << endl ;
        cerr << "//     Discriminant offset : " << proto.getStruct().getDiscriminantOffset() << endl ;
        for (const auto& field: structschema.getFields()) {
            const auto fproto = field.getProto() ;
            if (fproto.which() == Field::SLOT) {
                cerr << "//     Field " << fproto.getName().cStr() << endl ;
                cerr << "//         Index : " << field.getIndex() << endl ;
                cerr << "//         Offset : " << fproto.getSlot().getOffset() << endl ;
                cerr << "//         Code order : " << fproto.getCodeOrder() << endl ;
                cerr << "//         Discriminant value : " << fproto.getDiscriminantValue() << endl ;
                cerr << "//         " ; printTypeInfos(field.getType()) ;
            } else {
                cerr << "//     Group " << fproto.getName().cStr() << endl ;
            }
        }
    }
    */
    string indent = "" ;
    for (uint i = 0; i < level ; ++ i) {
        indent += tab ;
    }
    string subindent = indent + tab ;
    os << indent << "pub const " << name << " = struct {" << endl ;
    // Emit nested nodes.
    emitNestedNodes(os, loader, schema, level + 1) ;
    // Emit the unnamed union enum if the struct has overlapping fields.
    emitUnionEnum(os, loader, schema, subindent) ;
    // Reader.
    os << subindent << "pub const " << "View = struct {" << endl ;
    reader::emitStructFunctions(os, loader, structschema, subindent, name) ;
    os << subindent << "} ;" << endl ;
    // Writer.
    os << subindent << "pub const " << "Stencil = struct {" << endl ;
    os << subindent << tab << "message: *capnp.MessageBuffer," << endl ;
    os << subindent << tab << "segment: u32," << endl ;
    os << subindent << tab << "data: u32, // bytes offset in segment" << endl ;
    os << subindent << tab << "ptrs: u32, // bytes offset in segment" << endl ;
    writer::emitStructFunctions(os, loader, structschema, subindent, name) ;
    os << subindent << "} ;" << endl ;
    os << indent << "} ;" << endl ;
    os << endl ;
}

///
/// Emit an enum. This one is simple.
///
void emitEnum
    ( ostream& os
    , const SchemaLoader& loader
    , const Schema& schema
    , const char* name
    , const uint level
    )
{
    string indent = "" ;
    for (uint i = 0; i < level ; ++ i) {
        indent += tab ;
    }
    // Simply emit all enumerants. The correct order (w.r.t the @i index)
    // seems to be handled correctly directly in the AST. No checking here.
    // FIXME Ask the question, maybe...
    os << indent << "pub const " << name << " = enum(u16) {" << endl ;
    for (const auto& enumerant: schema.asEnum().getEnumerants()) {
        os << indent << tab << enumerant.getProto().getName().cStr() << "," << endl ;
    }
    os << indent << "} ;" << endl ;
    os << endl ;
} ;

///
/// ...
///
void emitNestedNodes
    ( ostream& os
    , const SchemaLoader& loader
    , const Schema& node_schema
    , const uint level
    )
{
    for (const auto& nested: node_schema.getProto().getNestedNodes()) {
        const auto name = nested.getName().cStr() ;
        const auto schema = loader.getUnbound(nested.getId()) ;
        const auto proto = schema.getProto() ;
        if (proto.isStruct()) {
            emitStruct(os, loader, schema, name, level) ;
        } else if (proto.isEnum()) {
            emitEnum(os, loader, schema, name, level) ;
        } else if (proto.isInterface()) {
            cerr << "Went over an interface node. Skipping..." << endl ;
        } else if (proto.isConst()) {
            cerr << "Went over an constant node. Skipping..." << endl ;
        } else if (proto.isAnnotation()) {
            cerr << "Went over an annotation node. Skipping..." << endl ;
        } else {
            cerr << "Some node is not what it is supposed to be." << endl ;
        }
    }
}

///
/// Emit an epilogue to the zig file.
///
void emitEpilogue(ostream& os) {
    // Nothing.
}

///
/// ...
///
void emitFile
    ( const SchemaLoader& loader
    , const size_t id
    )
{
    const auto schema = loader.get(id) ;
    ostream os(std::cout.rdbuf()) ;
    emitPrologue(os) ;
    emitNestedNodes(os, loader, schema) ;
    emitEpilogue(os) ;
}

///
/// General function emitting the zig file.
///
ErrorCode generateFrom(int fd)
{
    capnp::StreamFdMessageReader message(fd, {}) ;
    auto request = message.getRoot<Request>() ;
    SchemaLoader schema_loader ;
    for (auto node: request.getNodes()) {
        schema_loader.load(node);
    }
    /*
    // Debug...
    for (auto node: request.getNodes()) {
        cerr << "// node with id " << node.getId() << " is " << node.getDisplayName().cStr() << endl ;
        for (const auto& nested: node.getNestedNodes()) {
            cerr << "//     " << nested.getId() << " is " << nested.getName().cStr() << endl ;
        }
    }
    */
    for (const auto& requested_file: request.getRequestedFiles()) {
        emitFile(schema_loader, requested_file.getId());
    }
    return ErrorCode::Ok ;
}

int main()
{
    return static_cast<int>(generateFrom(STDIN_FILENO)) ;
}
