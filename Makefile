# This is a trivially readable makefile. I barely use it, its sole purpose
# is to demonstrate what workflow I use to develop the program. It will
# probably sound stupid but since I started using zig in 2018 I have never
# tried to use the zig build system (it was clunky back then).
#
# Anyway, I like that makefiles are extremely common (at least for linux
# users...) ; since the program is not finalized at all compiling "by hand"
# still seems easier to me.
#
# Be sure to leave this ONESHELL option. It requires makefile to do every
# commands in a single shell instance, which is important for the various
# cd (this is a magick trick to have Make be a convenient shell script).

.ONESHELL:

all: plugin generation tests

plugin:
	mkdir -p build
	cd build
	meson ..
	ninja
	cd ..

generation: plugin
	cd build
	# Generate zig code for our examples. I redirect to /dev/null for the
	# purpose of the script. When actually debugging I have it printed on
	# the console.
	capnpc ../tests/basic.capnp -o-      | ./capnpc-zig-alt > ../tests/basic.zig        2>&1
	capnpc ../tests/primitives.capnp -o- | ./capnpc-zig-alt > ../tests/primitives.zig   2>&1
	capnpc ../tests/lists.capnp -o-      | ./capnpc-zig-alt > ../tests/lists.zig        2>&1
	capnpc ../tests/unions.capnp -o-     | ./capnpc-zig-alt > ../tests/unions.zig       2>&1
	# Create reference binaries to compare to.
	cd ../tests
	cat basic.json       | capnp convert json:binary basic.capnp A                > basic,ref.bin
	cat primitives.json  | capnp convert json:binary primitives.capnp Foo         > primitives,ref.bin
	cat lists.json       | capnp convert json:binary lists.capnp ListOfListOfList > lists,ref.bin
	cat unions.json      | capnp convert json:binary unions.capnp Outer             > unions,ref.bin
	cd ..

tests: generation
	cd tests
	zig test tests.zig -lc --pkg-begin capnp '../src/capnp.zig' --pkg-end --test-filter 'union'
	#zig test tests.zig -lc --pkg-begin capnp '../src/capnp.zig' --pkg-end
	cd ..
	#cd src
	#zig test capnp.zig -lc
	#cd ..

clean:
	rm -f tests/*.bin
	rm -f tests/primitives.zig
	rm -f tests/lists.zig
	rm -f tests/unions.zig
	cd build
	ninja clean
	cd ..
