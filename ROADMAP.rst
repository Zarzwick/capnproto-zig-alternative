======================
La feuille de la route
======================

This roadmap is conservative ; some elements not ticked might already be started.
Elements marked as WIP are the one on which I'm currently working.
Elements ticked are considered complete, with corresponding tests.

Serialization/deserialization
=============================

-------------
Low-level API
-------------

Encoding
--------

- [x] Regular word
- [x] Zero region
- [ ] Non-zero region
- [ ] Tests

Decoding
--------

- [x] Regular word
- [x] Zero region
- [ ] Non-zero region
- [ ] Tests

Serialization
-------------

- [x] Primitives
- [x] Structures
- [ ] Enums
- [x] Lists
- [ ] Blobs
- [ ] Groups
- [ ] Unions
- [ ] Inter-segment pointers
- [ ] Strings

Deserialization
---------------

- [ ] Primitives
- [ ] Structures
- [ ] Enums
- [ ] Lists
- [ ] Blobs
- [ ] Inter-segment pointers
- [ ] Strings

--------------
High-level API
--------------

- [ ] Think about it

RPC
===

- [ ] Think about it
