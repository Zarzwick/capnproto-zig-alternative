@0xd62ae7d3f9df4444 ;

enum Truc {
	nuche  @0 ;
	machin @1 ;
	chouette @4 ;
	bidule @2 ;
	chose  @3 ;
}

struct NestedInBar {
	f @0 : Int64 ;
}

struct Bar {
	a @0 : Int32 ;
	b @1 : UInt16 ;
	c @2 : Float64 ;
	nested0 @3 : NestedInBar ;
	nested1 @4 : NestedInBar ;
}

struct Foo {
	d @0 : Int64 ;
	bar @1 : Bar ;
	e0 @2 : Bool ;
	e1 @3 : Bool ;
	e2 @4 : Bool ;
	e3 @5 : Bool ;
	e4 @6 : Bool ;
	e5 @7 : Bool ;
	g @8 : Truc ;
}
